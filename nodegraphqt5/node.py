# -*- coding: utf-8 -*-


"""A node representing a Qt widget.

:author: Guillaume Plum.
"""


from abc import ABCMeta


class Node(object):
    """A graph node."""

    def __init__(self, *args, **kwargs):
        """Construct a Node."""
        #  super(Node, self).__init__()
        self._connected = list()

    def add_connection(self, node):
        """Add a connection to the node.

        :param node: node connection to add.
        :type node: Node
        """
        if node in self._connected(node):
            self._connected.append(node)

        return self

    def delete_connection(self, node):
        """Remove a connection from the node.

        :param node: node connection to remove.
        :type node: Node
        """
        if node in self._connected:
            self._connected.remove(node)

        return self
