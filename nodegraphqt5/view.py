# -*- coding: utf-8 -*-

"""Qt views.

:author: Guillaume Plum.
"""


from PyQt5.QtWidgets import QGraphicsItem

from .node import Node


class NodeView(QGraphicsItem, Node):
    """Show Node object."""

    def __init__(self, parent=None):
        """Node viewer."""
        super(NodeView, self).__init__(parent)

        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
