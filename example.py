#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys

from nodegraphqt5.view import NodeView
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView

from widget import Ui_Form as Example


class Test(QWidget, Example):
    """Nieh."""
    def __init__(self, parent=None):
        super(Test, self).__init__(parent)
        self.setupUi(self)


app = QApplication(sys.argv)
scene = QGraphicsScene()

n = NodeView()
euh = Test()

scene.addText("Just a test")
scene.addItem(n)
scene.addWidget(euh)

view = QGraphicsView(scene)
view.setCacheMode(QGraphicsView.CacheBackground)
view.setRenderHint(QPainter.Antialiasing)
view.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
view.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
view.setDragMode(QGraphicsView.RubberBandDrag)
view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

view.show()

sys.exit(app.exec_())
