#! /usr/bin/env python3
# -*- coding:Utf8 -*-

"""Build Node Graph widget layout for PyQt5.

:author: Guillaume Plum
"""

# -------------------------------------------------------------------------------------------------------------
# All necessary import:
# -------------------------------------------------------------------------------------------------------------
import datetime as dt
import glob
import os
import os.path as op
import setuptools as st
import subprocess as sp

#  from datetime.datetime import fromtimestamp as fts
from distutils.core import setup
from distutils.command.install_data import install_data
from setuptools.command.develop import develop
from setuptools.command.build_py import build_py
from setuptools.command.install import install
from shutil import which


def test_dependency(*modules_list):
    """Test if an import is available.

    :param *modules_list: List of module to test existence.

    :returns: True if all are available, False and a list of missing modules
        else.
    """
    import imp

    missing = list()

    for deps in modules_list:
        try:
            imp.find_module(deps)
        except ImportError:
            missing.append(deps)

    if len(missing) != 0:
        return False, missing

    return True, None


def get():
    """Return the pyuic5 executable.

    :returns: The path to the 'pyuic5' executable.
    :raises: FileNotFoundError if not found.
    """
    prog = which("pyuic5")

    if prog is None:
        raise FileNotFoundError("'pyuic5' executable is not available.")

    return prog


def to_compile(src, output):
    """Check if src is more recent than output.

    :param src: Original file to compare.
    :type src: str
    :param output: Output file.
    :type output: str

    :returns: True if Original is more recent than output, else False.
    """
    if not op.exists(output):
        return True

    return dt.datetime.fromtimestamp(op.getmtime(src)) >= dt.datetime.fromtimestamp(op.getmtime(output))


def get_ui_files(module):
    """Search for all ui files in the modules.

    :param module: Module to search in.
    :type module: str or Path

    :returns: List of ui files.
    """
    ui_files = list()
    for root, dirs, files in os.walk(module):
        for f in files:
            print(f, op.splitext(f)[1])
            if op.splitext(f)[1] == '.ui':
                ui_files.append(
                    op.join(
                        root,
                        f
                    )
                )

    return ui_files


def pyuic(arg):
    """Build Qt ui components.

    Use as a class decorator to overload some classes and build all Qt ui
    components.
    To do this, it while overwrite the run method of the given to first
    build the ui components then run the original method.

    :param args: Class to overload.

    :returns: Overloaded class.
    """
    old_run = arg.run

    def run(*args, **kwargs):
        pyuic5 = get()
        ui_files = get_ui_files("nodegraphqt5")

        for uif in ui_files:
            out = op.join(
                op.splitext(uif)[0] + '_ui.py'
            )

            if to_compile(uif, out):
                print("Compiling", uif, "-->", out)
                sp.run([pyuic5, uif, "-o", out], check=True)

        old_run(*args, **kwargs)

    arg.run = run

    return arg


@pyuic
class build_pyuic(build_py):
    """Overload the build target to also build Qt ui components."""

    pass


@pyuic
class install_pyuic(install):
    """Overload the install target to also build Qt ui components."""

    pass


@pyuic
class develop_pyuic(develop):
    """Overload the develop target to also build Qt ui components."""

    pass


packages = st.find_packages()
cmdclass = {
    'build_py': build_pyuic,
    'install': install_pyuic,
    'develop': develop_pyuic,
    'install_data': install_data
}

# -------------------------------------------------------------------------------------------------------------
# Call the setup function:
# -------------------------------------------------------------------------------------------------------------
setup(
    name='nodegraphqt5',
    version='0.0.1a',
    description='Node graph layout for widget.',
    author='Guillaume Plum',
    packages=packages,
    cmdclass=cmdclass,
    include_package_data=True,
    package_data={
        'nodegraphqt5': glob.glob("data/*", recursive=True)
    },
    scripts=glob.glob("scripts/*"),
    install_requires=['pyqt5'],
)

# vim:spelllang=
